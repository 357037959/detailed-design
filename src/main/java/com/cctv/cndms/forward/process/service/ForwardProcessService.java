package com.cctv.cndms.forward.process.service;

import com.cctv.cndms.common.dto.ForwardInfo;
import com.cctv.cndms.enums.ResultState;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:55
 */
public interface ForwardProcessService {

    ResultState forwardProcess(ForwardInfo forwardInfo);

}
