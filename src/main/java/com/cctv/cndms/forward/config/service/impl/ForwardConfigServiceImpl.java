package com.cctv.cndms.forward.config.service.impl;

import com.cctv.cndms.domain.ForwardConfig;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.forward.config.condition.ForwardConfigCondition;
import com.cctv.cndms.forward.config.dto.ForwardConfigDTO;
import com.cctv.cndms.forward.config.service.ForwardConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:51
 */
@Service
public class ForwardConfigServiceImpl implements ForwardConfigService {
    @Override
    public ResultState saveForwardConfig(ForwardConfigDTO forwardConfigDTO) {
        return null;
    }

    @Override
    public ResultState updateForwardConfig(ForwardConfigDTO forwardConfigDTO) {
        return null;
    }

    @Override
    public ResultState deleteForwardConfig(Integer forwardConfigId) {
        return null;
    }

    @Override
    public ForwardConfigDTO findForwardConfig(Integer forwardConfigId) {
        return null;
    }

    @Override
    public List<ForwardConfigDTO> findForwardConfigList(ForwardConfigCondition forwardConfigCondition) {
        return null;
    }
}
