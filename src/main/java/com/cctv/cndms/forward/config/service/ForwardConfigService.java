package com.cctv.cndms.forward.config.service;

import com.cctv.cndms.domain.ForwardConfig;
import com.cctv.cndms.enums.ResultState;

import com.cctv.cndms.forward.config.condition.ForwardConfigCondition;
import com.cctv.cndms.forward.config.dto.ForwardConfigDTO;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:42
 */
public interface ForwardConfigService {

    ResultState saveForwardConfig(ForwardConfigDTO forwardConfigDTO);

    ResultState updateForwardConfig(ForwardConfigDTO forwardConfigDTO);

    ResultState deleteForwardConfig(Integer forwardConfigId);

    ForwardConfigDTO findForwardConfig(Integer forwardConfigId);

    List<ForwardConfigDTO> findForwardConfigList(ForwardConfigCondition forwardConfigCondition);

}
