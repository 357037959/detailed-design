package com.cctv.cndms.forward.config.controller;

import com.cctv.cndms.common.dto.Result;
import com.cctv.cndms.forward.config.condition.ForwardConfigCondition;
import com.cctv.cndms.forward.config.dto.ForwardConfigDTO;
import com.cctv.cndms.forward.config.service.ForwardConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:43
 */
@RestController
@RequestMapping("forword")
public class ForwardConfigController {

    @Autowired
    private ForwardConfigService forwardConfigService;

    @PostMapping("config")
    public Result saveForwardConfig(@RequestBody ForwardConfigDTO forwardConfigDTO) {
        forwardConfigService.saveForwardConfig(forwardConfigDTO);
        return Result.SUCCESS_RESULT;
    }

    @PutMapping("config")
    public Result updateForwardConfig(@RequestBody ForwardConfigDTO forwardConfigDTO) {
        forwardConfigService.updateForwardConfig(forwardConfigDTO);
        return Result.SUCCESS_RESULT;
    }

    @DeleteMapping("config/{configId}")
    public Result deleteForwardConfig(@PathVariable("configId") Integer configId) {
        forwardConfigService.deleteForwardConfig(configId);
        return Result.SUCCESS_RESULT;
    }

    @GetMapping("config/{configId}")
    public Result findForwardConfig(@PathVariable("configId") Integer configId) {
        ForwardConfigDTO forwardConfig = forwardConfigService.findForwardConfig(configId);
        return Result.SUCCESS_RESULT;
    }

    @GetMapping("config")
    public Result findForwardConfig(@RequestParam ForwardConfigCondition forwardConfigCondition) {
        List<ForwardConfigDTO> forwardConfigDTOList = forwardConfigService.findForwardConfigList(forwardConfigCondition);
        return Result.SUCCESS_RESULT;
    }

}
