package com.cctv.cndms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DetailedDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(DetailedDesignApplication.class, args);
    }
}
