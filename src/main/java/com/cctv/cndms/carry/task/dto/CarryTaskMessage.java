package com.cctv.cndms.carry.task.dto;

import com.cctv.cndms.common.consumer.Message;
import com.cctv.cndms.common.entiry.IEntity;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 22:29
 */
public class CarryTaskMessage implements IEntity, Message {
    @Override
    public String getMessageId() {
        return null;
    }
}
