package com.cctv.cndms.carry.task.consumer.parser;

import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.common.consumer.Message;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:20
 */
public interface CarryParser {

    ResultState parseMessage(CarryTaskMessage carryTaskMessage);

}
