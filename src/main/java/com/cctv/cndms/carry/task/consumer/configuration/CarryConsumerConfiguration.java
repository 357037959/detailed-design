package com.cctv.cndms.carry.task.consumer.configuration;

import com.cctv.cndms.carry.task.consumer.CarryConsumer;
import com.cctv.cndms.carry.task.consumer.parser.CarryParser;
import com.cctv.cndms.common.consumer.ThreadPoolTaskExecutors;
import com.cctv.cndms.transcode.consumer.TranscodeConsumer;
import com.cctv.cndms.transcode.parser.TranscodeParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @category 消费者配置
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/2/27 17:14
 */
@Configuration
public class CarryConsumerConfiguration {

    @Autowired
    private CarryParser carryParser;

    @Bean
    public ThreadPoolTaskExecutor carryConsumerThreadPool() {
        ThreadPoolTaskExecutor consumerThreadPool = ThreadPoolTaskExecutors.createThreadPoolTaskExecutor();
        consumerThreadPool.execute(new CarryConsumer(carryParser));
        return consumerThreadPool;
    }

}
