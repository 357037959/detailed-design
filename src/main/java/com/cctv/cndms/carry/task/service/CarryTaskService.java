package com.cctv.cndms.carry.task.service;

import com.cctv.cndms.carry.task.condition.CarryTaskCondition;
import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.domain.CarryTask;
import com.cctv.cndms.enums.ResultState;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 22:27
 */
public interface CarryTaskService {

    ResultState createTask(CarryTaskMessage carryTaskMessage);

    ResultState	executeTask(String origMd5, String origPath, String destPath);

    ResultState	checkMd5(String origMd5, String destPath);

    List<CarryTask> findTaskList(CarryTaskCondition carryTaskCondition);

    ResultState	feedbackTask(CarryTask carryTask);

    ResultState	retryTask(Long taskId);

}
