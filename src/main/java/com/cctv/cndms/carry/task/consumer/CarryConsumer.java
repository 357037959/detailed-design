package com.cctv.cndms.carry.task.consumer;

import com.cctv.cndms.carry.task.consumer.parser.CarryParser;
import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.common.consumer.BaseConsumer;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.common.consumer.ConsumerConstants;
import lombok.extern.slf4j.Slf4j;

/**
 * @category 搬运消息消费者
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/12 15:32
 */
@Slf4j
public class CarryConsumer extends BaseConsumer<CarryTaskMessage> {

    protected CarryParser carryParser;

    public CarryConsumer(CarryParser carryParser) {
        this.messageQueue = ConsumerConstants.CARRY_QUEUE;
        this.carryParser = carryParser;
    }

    protected ResultState parseMessage(CarryTaskMessage carryTaskMessage) {
        try {
            ResultState resultState = carryParser.parseMessage(carryTaskMessage);
            log.info("搬运消息消费者处理消息, messageId={}", carryTaskMessage.getMessageId());
            return resultState;
        } catch (Exception e) {
            log.error("搬运消息消费者消费异常", e);
        }
        return ResultState.Failure;
    }

}
