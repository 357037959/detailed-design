package com.cctv.cndms.carry.task.consumer.parser.impl;

import com.cctv.cndms.carry.task.consumer.parser.CarryParser;
import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.carry.task.service.CarryTaskService;
import com.cctv.cndms.common.consumer.Message;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Service
public class CarryParserImpl implements CarryParser {

    @Autowired
    private CarryTaskService carryTaskService;

    @Override
    public ResultState parseMessage(CarryTaskMessage carryTaskMessage) {
        return ResultState.Success;
    }
}
