package com.cctv.cndms.carry.task.service.impl;

import com.cctv.cndms.carry.task.condition.CarryTaskCondition;
import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.carry.task.service.CarryTaskService;
import com.cctv.cndms.domain.CarryTask;
import com.cctv.cndms.enums.ResultState;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 22:58
 */
@Service
public class CarryTaskServiceImpl implements CarryTaskService {
    @Override
    public ResultState createTask(CarryTaskMessage carryTaskMessage) {
        CarryTask carryTask = new CarryTask();
        BeanUtils.copyProperties(carryTaskMessage, carryTask);
        String origMd5 = "";
        executeTask("", "", "");
        checkMd5("", "");
        feedbackTask(carryTask);
        return null;
    }

    @Override
    public ResultState executeTask(String origMd5, String origPath, String destPath) {
        return null;
    }

    @Override
    public ResultState checkMd5(String origMd5, String destPath) {
        return null;
    }

    @Override
    public List<CarryTask> findTaskList(CarryTaskCondition carryTaskCondition) {
        return null;
    }

    @Override
    public ResultState feedbackTask(CarryTask carryTask) {
        return null;
    }

    @Override
    public ResultState retryTask(Long taskId) {
        return null;
    }
}
