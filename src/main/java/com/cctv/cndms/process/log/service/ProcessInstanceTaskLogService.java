package com.cctv.cndms.process.log.service;

import com.cctv.cndms.domain.ProcessInstanceTaskLog;
import com.cctv.cndms.process.log.condition.ProcessInstanceTaskLogCondition;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 16:03
 */
public interface ProcessInstanceTaskLogService {

    List<ProcessInstanceTaskLog> findProcessInstanceTaskLogList(ProcessInstanceTaskLogCondition processInstanceTaskLogCondition);

}
