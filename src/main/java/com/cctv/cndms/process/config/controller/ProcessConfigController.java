package com.cctv.cndms.process.config.controller;

import com.cctv.cndms.common.dto.Result;
import com.cctv.cndms.process.config.dto.ProcessConfigDTO;
import com.cctv.cndms.process.config.service.ProcessConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:12
 */
@RestController("process/config")
public class ProcessConfigController {

    @Autowired
    private ProcessConfigService processConfigService;

    public Result saveProcessConfig(@RequestBody ProcessConfigDTO processConfigDTO) {
        processConfigService.saveProcessConfig(processConfigDTO);
        return Result.SUCCESS_RESULT;
    }

}
