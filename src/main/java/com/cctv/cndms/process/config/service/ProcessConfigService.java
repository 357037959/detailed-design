package com.cctv.cndms.process.config.service;

import com.cctv.cndms.domain.ProcessConfig;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.process.config.condition.ProcessConfigCondition;
import com.cctv.cndms.process.config.dto.ProcessConfigDTO;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:13
 */
public interface ProcessConfigService {

    ResultState saveProcessConfig(ProcessConfigDTO processConfigDTO);

    ResultState updateProcessConfig(ProcessConfigDTO processConfigDTO);

    ResultState deleteProcessConfig(Integer configId);

    ProcessConfig findProcessConfig(Integer configId);

    List<ProcessConfig> findProcessConfigList(ProcessConfigCondition processConfigCondition);

    ResultState setProcessConfigState(Integer configId, Boolean enabled);

}
