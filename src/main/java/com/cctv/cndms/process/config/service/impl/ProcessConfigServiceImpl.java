package com.cctv.cndms.process.config.service.impl;

import com.cctv.cndms.domain.ProcessConfig;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.process.config.condition.ProcessConfigCondition;
import com.cctv.cndms.process.config.dto.ProcessConfigDTO;
import com.cctv.cndms.process.config.service.ProcessConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:18
 */
@Service
public class ProcessConfigServiceImpl implements ProcessConfigService {
    @Override
    public ResultState saveProcessConfig(ProcessConfigDTO processConfigDTO) {
        return null;
    }

    @Override
    public ResultState updateProcessConfig(ProcessConfigDTO processConfigDTO) {
        return null;
    }

    @Override
    public ResultState deleteProcessConfig(Integer configId) {
        return null;
    }

    @Override
    public ProcessConfig findProcessConfig(Integer configId) {
        return null;
    }

    @Override
    public List<ProcessConfig> findProcessConfigList(ProcessConfigCondition processConfigCondition) {
        return null;
    }

    @Override
    public ResultState setProcessConfigState(Integer configId, Boolean enabled) {
        return null;
    }
}
