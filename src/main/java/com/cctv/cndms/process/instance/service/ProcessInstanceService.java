package com.cctv.cndms.process.instance.service;

import com.cctv.cndms.common.dto.ForwardInfo;
import com.cctv.cndms.domain.ForwardProcess;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:21
 */
public interface ProcessInstanceService {

    ResultState parseForwardInfo(ForwardInfo forwardInfo, List<ForwardProcess> forwardProcessList);

    ResultState startProcessTask(ProcessMessage processMessage);

    ResultState feedbackProcessTask(ProcessMessage processMessage);

    ResultState retryProcess (ProcessMessage processMessage);

    ResultState reuseProcess(ProcessMessage processMessage);

}
