package com.cctv.cndms.process.instance.service.impl;

import com.cctv.cndms.common.dto.ForwardInfo;
import com.cctv.cndms.domain.ForwardProcess;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.process.instance.service.ProcessInstanceService;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 15:29
 */
public class ProcessInstanceServiceImpl implements ProcessInstanceService {

    @Override
    public ResultState parseForwardInfo(ForwardInfo forwardInfo, List<ForwardProcess> forwardProcessList) {
        return null;
    }

    @Override
    public ResultState startProcessTask(ProcessMessage processMessage) {
        return null;
    }

    @Override
    public ResultState feedbackProcessTask(ProcessMessage processMessage) {
        return null;
    }

    @Override
    public ResultState retryProcess(ProcessMessage processMessage) {
        return null;
    }

    @Override
    public ResultState reuseProcess(ProcessMessage processMessage) {
        return null;
    }

}
