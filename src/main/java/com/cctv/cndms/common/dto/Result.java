package com.cctv.cndms.common.dto;

import com.cctv.cndms.common.entiry.IEntity;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 14:57
 */
public class Result implements IEntity {

    public static Result SUCCESS_RESULT = new Result();

    public static Result FAIL_RESULT = new Result();

}
