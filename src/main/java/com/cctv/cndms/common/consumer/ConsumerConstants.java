package com.cctv.cndms.common.consumer;

import com.cctv.cndms.common.consumer.queue.ConsumerBlockingQueue;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 17:05
 */
public class ConsumerConstants {

    /** @category 视频队列 */
    public static final ConsumerBlockingQueue VIDEO_QUEUE = new ConsumerBlockingQueue();
    /** @category 编排单队列 */
    public static final ConsumerBlockingQueue FORM_QUEUE = new ConsumerBlockingQueue();
    /** @category 导航队列 */
    public static final ConsumerBlockingQueue NAVIGATION_QUEUE = new ConsumerBlockingQueue();
    /** @category 下架队列 */
    public static final ConsumerBlockingQueue REMOVED_QUEUE = new ConsumerBlockingQueue();
    /** @category 搬运队列 */
    public static final ConsumerBlockingQueue CARRY_QUEUE = new ConsumerBlockingQueue();
    /** @category 转码队列 */
    public static final ConsumerBlockingQueue TRANSCODE_QUEUE = new ConsumerBlockingQueue();
    /** @category 加密队列 */
    public static final ConsumerBlockingQueue ENCRYPT_QUEUE = new ConsumerBlockingQueue();

}
