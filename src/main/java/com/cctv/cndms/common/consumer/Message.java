package com.cctv.cndms.common.consumer;

/**
 * @category 消息接口
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/23 1:24
 */
public interface Message {

    String getMessageId();

}
