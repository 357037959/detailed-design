package com.cctv.cndms.domain;

import com.cctv.cndms.common.entiry.IEntity;
import com.cctv.cndms.common.consumer.Message;
import lombok.Data;

import java.util.Date;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 16:51
 */
@Data
public class UpstreamCMSMessage implements Message, IEntity {

    /** @category 消息主键 */
    private Integer id;
    /** @category 消息ID */
    private String messageUUID;
    /** @category 消息ID */
    private String messageId;
    /** @category 消息类型 */
    private String messageType;
    /** @category 消息标题 */
    private String messageTitle;
    /** @category 消息内容 */
    private String messageContent;
    /** @category 创建时间 */
    private Date createTime;
    /** @category 修改时间 */
    private Date lastmodifyTime;
    /** @category 处理状态（0-未处理，1-已处理） */
    private Integer state;

    @Override
    public String getMessageId() {
        return null;
    }
}
