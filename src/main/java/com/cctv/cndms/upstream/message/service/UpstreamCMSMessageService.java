package com.cctv.cndms.upstream.message.service;

import com.cctv.cndms.enums.RecordState;
import com.cctv.cndms.enums.ResultState;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 16:55
 */
public interface UpstreamCMSMessageService {

    ResultState saveVideoCMSMessage(String message);

    ResultState saveFormCMSMessage(String message);

    ResultState saveNavigationCMSMessage(String message);

    ResultState saveRemovedCMSMessage(String message);

    ResultState updateCMSMessageState(Integer id, RecordState recordState);

}
