package com.cctv.cndms.upstream.message.consumer.parser;

import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.common.consumer.queue.ConsumerBlockingQueue;
import com.cctv.cndms.domain.UpstreamCMSMessage;
import com.cctv.cndms.enums.ResultState;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:20
 */
public interface UpstreamParser {

    ResultState parseMessage(UpstreamCMSMessage upstreamCMSMessage);

    ConsumerBlockingQueue getMessageQueue();

}
