package com.cctv.cndms.upstream.message.consumer.parser.impl;

import com.cctv.cndms.carry.task.dto.CarryTaskMessage;
import com.cctv.cndms.carry.task.service.CarryTaskService;
import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.domain.UpstreamCMSMessage;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.upstream.message.consumer.parser.UpstreamParser;
import com.cctv.cndms.upstream.message.service.UpstreamCMSMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Service("navigationUpstreamParser")
public class NavigationUpstreamParserImpl extends BaseUpstreamParserImpl {

    public NavigationUpstreamParserImpl() {
        super(ConsumerConstants.NAVIGATION_QUEUE);
    }

    @Override
    public ResultState parseMessage(UpstreamCMSMessage upstreamCMSMessage) {
        return ResultState.Success;
    }

}
