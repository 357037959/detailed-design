package com.cctv.cndms.upstream.message.consumer.configuration;

import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.common.consumer.ThreadPoolTaskExecutors;
import com.cctv.cndms.upstream.message.consumer.UpstreamConsumer;
import com.cctv.cndms.upstream.message.consumer.parser.UpstreamParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @category 消费者配置
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/2/27 17:14
 */
@Configuration
public class UpstreamConsumerConfiguration {

    @Autowired
    @Qualifier("videoUpstreamParser")
    private UpstreamParser videoUpstreamParser;
    @Autowired
    @Qualifier("formUpstreamParser")
    private UpstreamParser formUpstreamParser;
    @Autowired
    @Qualifier("navigationUpstreamParser")
    private UpstreamParser navigationUpstreamParser;
    @Autowired
    @Qualifier("removedUpstreamParser")
    private UpstreamParser removedUpstreamParser;

    @Bean
    public ThreadPoolTaskExecutor upstreamConsumerThreadPool() {
        ThreadPoolTaskExecutor consumerThreadPool = ThreadPoolTaskExecutors.createThreadPoolTaskExecutor();
        consumerThreadPool.execute(new UpstreamConsumer(videoUpstreamParser));
        consumerThreadPool.execute(new UpstreamConsumer(formUpstreamParser));
        consumerThreadPool.execute(new UpstreamConsumer(navigationUpstreamParser));
        consumerThreadPool.execute(new UpstreamConsumer(removedUpstreamParser));
        return consumerThreadPool;
    }

}
