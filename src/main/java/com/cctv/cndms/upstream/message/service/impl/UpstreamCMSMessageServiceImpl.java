package com.cctv.cndms.upstream.message.service.impl;

import com.cctv.cndms.enums.RecordState;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.upstream.message.service.UpstreamCMSMessageService;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/10 17:09
 */
@Service
public class UpstreamCMSMessageServiceImpl implements UpstreamCMSMessageService {
    @Override
    public ResultState saveVideoCMSMessage(String message) {
        return null;
    }

    @Override
    public ResultState saveFormCMSMessage(String message) {
        return null;
    }

    @Override
    public ResultState saveNavigationCMSMessage(String message) {
        return null;
    }

    @Override
    public ResultState saveRemovedCMSMessage(String message) {
        return null;
    }

    @Override
    public ResultState updateCMSMessageState(Integer id, RecordState recordState) {
        return null;
    }
}
