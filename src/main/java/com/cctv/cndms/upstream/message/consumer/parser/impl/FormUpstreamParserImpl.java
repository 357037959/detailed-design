package com.cctv.cndms.upstream.message.consumer.parser.impl;

import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.domain.UpstreamCMSMessage;
import com.cctv.cndms.enums.ResultState;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Service("formUpstreamParser")
public class FormUpstreamParserImpl extends BaseUpstreamParserImpl {

    public FormUpstreamParserImpl() {
        super(ConsumerConstants.FORM_QUEUE);
    }

    @Override
    public ResultState parseMessage(UpstreamCMSMessage upstreamCMSMessage) {

        return ResultState.Success;
    }
    
}
