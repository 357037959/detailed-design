package com.cctv.cndms.upstream.message.consumer;

import com.cctv.cndms.common.consumer.BaseConsumer;
import com.cctv.cndms.domain.UpstreamCMSMessage;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.upstream.message.consumer.parser.UpstreamParser;
import lombok.extern.slf4j.Slf4j;

/**
 * @category 上游消息消费者
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/12 15:32
 */
@Slf4j
public class UpstreamConsumer extends BaseConsumer<UpstreamCMSMessage> {

    protected UpstreamParser upstreamParser;

    public UpstreamConsumer(UpstreamParser upstreamParser) {
        this.messageQueue = upstreamParser.getMessageQueue();
        this.upstreamParser = upstreamParser;
        this.upstreamParser.parseMessage(null);
    }

    protected ResultState parseMessage(UpstreamCMSMessage upstreamCMSMessage) {
        try {
            ResultState resultState = upstreamParser.parseMessage(upstreamCMSMessage);
            log.info("上游消息消费者处理消息, messageId={}", upstreamCMSMessage.getMessageId());
            return resultState;
        } catch (Exception e) {
            log.error("上游消息消费者消费异常", e);
        }
        return ResultState.Failure;
    }

}
