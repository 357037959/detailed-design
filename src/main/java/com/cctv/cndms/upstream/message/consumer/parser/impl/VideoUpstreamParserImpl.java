package com.cctv.cndms.upstream.message.consumer.parser.impl;

import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.common.consumer.queue.ConsumerBlockingQueue;
import com.cctv.cndms.domain.UpstreamCMSMessage;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.upstream.message.consumer.parser.UpstreamParser;
import com.cctv.cndms.upstream.message.service.UpstreamCMSMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Slf4j
@Service("videoUpstreamParser")
public class VideoUpstreamParserImpl extends BaseUpstreamParserImpl {

    public VideoUpstreamParserImpl() {
        super(ConsumerConstants.VIDEO_QUEUE);
    }

    @Override
    public ResultState parseMessage(UpstreamCMSMessage upstreamCMSMessage) {
        log.info("upstreamCMSMessageService={}", this.upstreamCMSMessageService);
        return ResultState.Success;
    }

}
