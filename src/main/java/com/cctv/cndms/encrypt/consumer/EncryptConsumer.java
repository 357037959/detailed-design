package com.cctv.cndms.encrypt.consumer;

import com.cctv.cndms.common.consumer.BaseConsumer;
import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.encrypt.parser.EncryptParser;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.parser.TranscodeParser;
import lombok.extern.slf4j.Slf4j;

/**
 * @category 转码消息消费者
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/12 15:32
 */
@Slf4j
public class EncryptConsumer extends BaseConsumer<ProcessMessage> {

    protected EncryptParser encryptParser;

    public EncryptConsumer(EncryptParser encryptParser) {
        this.messageQueue = ConsumerConstants.ENCRYPT_QUEUE;
        this.encryptParser = encryptParser;
        this.encryptParser.parseProcessMessage(new ProcessMessage());
    }

    protected ResultState parseMessage(ProcessMessage processMessage) {
        try {
            ResultState resultState = encryptParser.parseProcessMessage(processMessage);
            log.info("加密消息消费者处理消息, messageId={}", processMessage.getMessageId());
            return resultState;
        } catch (Exception e) {
            log.error("加密消息消费者消费异常", e);
        }
        return ResultState.Failure;
    }

}
