package com.cctv.cndms.encrypt.task.service;

import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.domain.EncryptParamerConfig;
import com.cctv.cndms.domain.EncryptServiceConfig;
import com.cctv.cndms.domain.EncryptTask;
import com.cctv.cndms.encrypt.task.condition.EncryptTaskCondition;
import com.cctv.cndms.enums.ResultState;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/12 10:56
 */
public interface EncryptTaskService {

    ResultState reuseTask(ProcessMessage processMessage, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig);

    ResultState	createTask(ProcessMessage processMessage, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig);

    ResultState	requestTask(EncryptTask encryptTask, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig);

    List<EncryptTask> findTaskList(EncryptTaskCondition encryptTaskCondition);

    ResultState	monitorTask();

    ResultState	retryTask(Long taskId);

    ResultState	feedbackTask(ProcessMessage processMessage);

}
