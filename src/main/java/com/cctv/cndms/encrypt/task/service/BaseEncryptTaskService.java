package com.cctv.cndms.encrypt.task.service;

import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.domain.EncryptParamerConfig;
import com.cctv.cndms.domain.EncryptServiceConfig;
import com.cctv.cndms.domain.EncryptTask;
import com.cctv.cndms.encrypt.task.condition.EncryptTaskCondition;
import com.cctv.cndms.enums.ResultState;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/12 0:14
 */
@Service
public abstract class BaseEncryptTaskService implements EncryptTaskService {
    @Override
    public ResultState reuseTask(ProcessMessage processMessage, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig) {
        return null;
    }

    @Override
    public ResultState createTask(ProcessMessage processMessage, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig) {
        return null;
    }

    @Override
    public List<EncryptTask> findTaskList(EncryptTaskCondition encryptTaskCondition) {
        return null;
    }

    @Override
    public ResultState retryTask(Long taskId) {
        return null;
    }

    @Override
    public ResultState feedbackTask(ProcessMessage processMessage) {
        return null;
    }
}
