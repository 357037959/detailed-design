package com.cctv.cndms.encrypt.task.service.impl;

import com.cctv.cndms.domain.EncryptParamerConfig;
import com.cctv.cndms.domain.EncryptServiceConfig;
import com.cctv.cndms.domain.EncryptTask;
import com.cctv.cndms.domain.TranscodeParamerConfig;
import com.cctv.cndms.domain.TranscodeServiceConfig;
import com.cctv.cndms.domain.TranscodeTask;
import com.cctv.cndms.encrypt.task.service.BaseEncryptTaskService;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.task.service.BaseTranscodeTaskService;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/12 0:14
 */
@Service("NovelSuperEncryptImplement")
public class NovelSuperEncryptTaskServiceImpl extends BaseEncryptTaskService {
    @Override
    public ResultState requestTask(EncryptTask encryptTask, EncryptServiceConfig encryptServiceConfig, EncryptParamerConfig encryptParamerConfig) {
        return null;
    }

    @Override
    public ResultState monitorTask() {
        return null;
    }
}
