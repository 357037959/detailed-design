package com.cctv.cndms.encrypt.parser.impl;

import com.cctv.cndms.common.ApplicationContextHelper;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.encrypt.parser.EncryptParser;
import com.cctv.cndms.encrypt.task.service.EncryptTaskService;
import com.cctv.cndms.enums.ResultState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Slf4j
@Service
public class EncryptParserImpl implements EncryptParser {

    private EncryptTaskService encryptTaskService;

    @Override
    public ResultState parseProcessMessage(ProcessMessage processMessage) {
        EncryptTaskService novelSuperEncryptImplement = ApplicationContextHelper.getBean("NovelSuperEncryptImplement", EncryptTaskService.class);
        log.info("EncryptTaskService={}", novelSuperEncryptImplement);
        return ResultState.Success;
    }
}
