package com.cctv.cndms.transcode.consumer;

import com.cctv.cndms.common.consumer.BaseConsumer;
import com.cctv.cndms.common.consumer.ConsumerConstants;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.parser.TranscodeParser;
import lombok.extern.slf4j.Slf4j;

/**
 * @category 转码消息消费者
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/12 15:32
 */
@Slf4j
public class TranscodeConsumer extends BaseConsumer<ProcessMessage> {

    protected TranscodeParser transcodeParser;

    public TranscodeConsumer(TranscodeParser transcodeParser) {
        this.messageQueue = ConsumerConstants.TRANSCODE_QUEUE;
        this.transcodeParser = transcodeParser;
        this.transcodeParser.parseProcessMessage(new ProcessMessage());
    }

    protected ResultState parseMessage(ProcessMessage processMessage) {
        try {
            ResultState resultState = transcodeParser.parseProcessMessage(processMessage);
            log.info("转码消息消费者处理消息, messageId={}", processMessage.getMessageId());
            return resultState;
        } catch (Exception e) {
            log.error("转码消息消费者消费异常", e);
        }
        return ResultState.Failure;
    }

}
