package com.cctv.cndms.transcode.parser;

import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.enums.ResultState;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:20
 */
public interface TranscodeParser {

    ResultState parseProcessMessage(ProcessMessage processMessage);

}
