package com.cctv.cndms.transcode.parser.impl;

import com.cctv.cndms.common.ApplicationContextHelper;
import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.encrypt.task.service.EncryptTaskService;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.parser.TranscodeParser;
import com.cctv.cndms.transcode.task.service.TranscodeTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:29
 */
@Slf4j
@Service
public class TranscodeParserImpl implements TranscodeParser {

    private TranscodeTaskService transcodeTaskService;

    @Override
    public ResultState parseProcessMessage(ProcessMessage processMessage) {
        TranscodeTaskService sobeyTranscodeImplement = ApplicationContextHelper.getBean("SobeyTranscodeImplement", TranscodeTaskService.class);
        log.info("TranscodeTaskService={}", sobeyTranscodeImplement);
        return ResultState.Success;
    }
}
