package com.cctv.cndms.transcode.task.service.impl;

import com.cctv.cndms.domain.TranscodeParamerConfig;
import com.cctv.cndms.domain.TranscodeServiceConfig;
import com.cctv.cndms.domain.TranscodeTask;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.task.service.BaseTranscodeTaskService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/12 0:14
 */
@Service("SobeyTranscodeImplement")
public class SobeyTranscodeTaskServiceImpl extends BaseTranscodeTaskService {
    @Override
    public ResultState requestTask(TranscodeTask transcodeTask, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig) {
        return null;
    }

    @Override
    public ResultState monitorTask() {
        return null;
    }
}
