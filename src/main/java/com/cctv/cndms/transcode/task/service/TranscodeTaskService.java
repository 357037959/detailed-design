package com.cctv.cndms.transcode.task.service;

import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.domain.TranscodeParamerConfig;
import com.cctv.cndms.domain.TranscodeServiceConfig;
import com.cctv.cndms.domain.TranscodeTask;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.task.condition.TranscodeTaskCondition;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/11 23:34
 */
public interface TranscodeTaskService {

    ResultState reuseTask(ProcessMessage processMessage, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig);

    ResultState	createTask(ProcessMessage processMessage, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig);

    ResultState	requestTask(TranscodeTask transcodeTask, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig);

    List<TranscodeTask> findTaskList(TranscodeTaskCondition transcodeTaskCondition);

    ResultState	monitorTask();

    ResultState retryTask(Long taskId);

    ResultState feedbackTask(ProcessMessage processMessage);
}
