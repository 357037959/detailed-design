package com.cctv.cndms.transcode.task.service;

import com.cctv.cndms.common.dto.ProcessMessage;
import com.cctv.cndms.domain.TranscodeParamerConfig;
import com.cctv.cndms.domain.TranscodeServiceConfig;
import com.cctv.cndms.domain.TranscodeTask;
import com.cctv.cndms.enums.ResultState;
import com.cctv.cndms.transcode.task.condition.TranscodeTaskCondition;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/9/12 0:14
 */
@Service
public abstract class BaseTranscodeTaskService implements TranscodeTaskService {
    @Override
    public ResultState reuseTask(ProcessMessage processMessage, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig) {
        return null;
    }

    @Override
    public ResultState createTask(ProcessMessage processMessage, TranscodeServiceConfig transcodeServiceConfig, TranscodeParamerConfig transcodeParamerConfig) {
        return null;
    }

    @Override
    public List<TranscodeTask> findTaskList(TranscodeTaskCondition transcodeTaskCondition) {
        return null;
    }

    @Override
    public ResultState retryTask(Long taskId) {
        return null;
    }

    @Override
    public ResultState feedbackTask(ProcessMessage processMessage) {
        return null;
    }
}
