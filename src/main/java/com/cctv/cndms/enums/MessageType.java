package com.cctv.cndms.enums;

/**
 * @category 消息类型
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/11 11:40
 */
public enum MessageType {

    Video("视频数据"),
    Form("编排单数据"),
    Navigation("导航数据"),
    Removed("下架数据"),
    Live("直播数据"),
    LiveRemoved("直播下架数据")
    ;

    public final String comment;

    MessageType(String comment) {
        this.comment = comment;
    }

}
