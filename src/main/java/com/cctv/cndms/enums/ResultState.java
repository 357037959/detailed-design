package com.cctv.cndms.enums;

/**
 * @category 处理结果状态
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/4/11 11:40
 */
public enum ResultState {

    Success(0, "处理成功"),
    Retry(1, "重试处理"),
    Failure(2, "处理失败")
    ;

    public final Integer code;
    public final String comment;

    ResultState(Integer code, String comment) {
        this.code = code;
        this.comment = comment;
    }

}
